from fastapi.testclient import TestClient
from LOMBARD_Nicolas_1_modele_012024 import app

client = TestClient(app)

def test_predict_positive_en():
    response = client.post("/predict/", json={"text": "@marcofolio they almost always make me smile at the start of the day  http://is.gd/12ndG,en,almost always make smile start day"})
    assert response.status_code == 200
    assert response.json()["prediction"] > 0.5
    assert response.json()["lang"] == "en"

def test_predict_negative_en():
    response = client.post("/predict/", json={"text": "Relaxing after cleaning up. Got to jump on a customer reference call @ 9:30PM. Will miss Entourage "})
    assert response.status_code == 200
    assert response.json()["prediction"] < 0.5
    assert response.json()["lang"] == "en"

def test_predict_empty_text():
    response = client.post("/predict/", json={"text": ""})
    assert response.status_code == 400
    assert response.json()["detail"] == "Text input is required."

def test_predict_special_characters():
    response = client.post("/predict/", json={"text": "( ͡° ͜ʖ ͡°) ≽ ^ • ⩊ • ^ ≼ (≧ヮ≦) 💕 ╾━╤de╦︻(▀̿ĺ̯▀̿̿)"})
    assert response.status_code == 200

def test_predict_long_text():
    long_text = "This is a really long text " * 1000
    response = client.post("/predict/", json={"text": long_text})
    assert response.status_code == 200

def test_predict_français_text():
    response = client.post("/predict/", json={"text": "Merci à la compagnie XYZ pour ce voyage parfait ! Des sièges confortables, des repas délicieux et un personnel souriant. À bientôt !"})
    assert response.status_code == 200
    assert response.json()["prediction"] > 0.5
    assert response.json()["lang"] == "fr"
def test_predict_français_text_neg():
    response = client.post("/predict/", json={"text": "Fuck la circulation ce mat1"})
    assert response.status_code == 200
    assert response.json()["prediction"] < 0.5
    assert response.json()["lang"] == "fr"

def test_predict_deutsch_text():
    response = client.post("/predict/", json={"text": "Großartige Erfahrung mit Fluggesellschaft XYZ! Pünktliche Flüge, freundliches Personal und ein reibungsloser Ablauf. Sehr empfehlenswert!"})
    assert response.status_code == 200
    assert response.json()["prediction"] > 0.5
    assert response.json()["lang"] == "de"
def test_predict_deutsch_text_neg():
    response = client.post("/predict/", json={"text": "Die Fluggesellschaft XYZ ist ein Albtraum! Verspätungen ohne Ende und miserabler Kundenservice. Nie wieder!"})
    assert response.status_code == 200
    assert response.json()["prediction"] < 0.5
    assert response.json()["lang"] == "de"

def test_predict_español_text():
    response = client.post("/predict/", json={"text": "¡Experiencia increíble con la aerolínea XYZ! Vuelos puntuales, atención al cliente excepcional y comodidad a bordo. ¡Definitivamente recomiendo!"})
    assert response.status_code == 200
    assert response.json()["prediction"] > 0.5
    assert response.json()["lang"] == "es"

def test_predict_português_text():
    response = client.post("/predict/", json={"text": "Adoro dias de sol!"})
    assert response.status_code == 200
    assert response.json()["lang"] == "pt"

def test_predict_italiano_text():
    response = client.post("/predict/", json={"text": "Adoro le giornate di sole!"})
    assert response.status_code == 200
    assert response.json()["lang"] == "it"

def test_predict_nederlands_text():
    response = client.post("/predict/", json={"text": "Ik hou van zonnige dagen!"})
    assert response.status_code == 200
    assert response.json()["lang"] == "nl"

def test_predict_россия_text():
    response = client.post("/predict/", json={"text": "Я люблю солнечные дни!"})
    assert response.status_code == 200
    assert response.json()["lang"] == "ru"

def test_predict_text_with_typos():
    response = client.post("/predict/", json={"text": "I lvoe snuny dysa!"})
    assert response.status_code == 200
    assert response.json()["lang"] == "en"

def test_predict_neutral_text():
    response = client.post("/predict/", json={"text": "It's an ordinary day."})
    assert response.status_code == 200
    assert response.json()["lang"] == "en"

def test_predict_text_with_social_media_elements():
    response = client.post("/predict/", json={"text": "Check out this cool event! #fun @event"})
    assert response.status_code == 200
    assert response.json()["lang"] == "en"
