# Détectez les Bad Buzz grâce au Deep Learning

Ce projet utilise le Deep Learning pour analyser des textes et déterminer s'ils ont une tonalité positive ou négative.
Il peut être particulièrement utile pour surveiller les réseaux sociaux et détecter les bad buzz potentiels.

## Fonctionnalités

- **Détection de langue** : Identifie la langue du texte soumis.
- **Prétraitement du texte** : Applique diverses techniques de nettoyage et de normalisation du texte pour le préparer à l'analyse NLP.
- **Analyse de sentiment** : Utilise un modèle de Deep Learning pour prédire si le texte a une tonalité positive ou négative.

## Prérequis

- Python 3.x
- Docker (pour le déploiement avec Docker)

## Installation

Clonez le dépôt :

```bash
git clone https://gitlab.com/openclassroom4150609/openclassroom-07-detectez-les-bad-buzz-grace-au-deep-learning.git
```
```bash
git submodule update --init --recursive
```

## Configuration de l'environnement

Créez et activez un environnement virtuel :

```bash
python -m venv venv
source venv/bin/activate  # Unix/MacOS
.\venv\Scripts\activate   # Windows
```

Installez les dépendances :

```bash
cd fastText && pip install --no-binary :all . && cd ..
```

```bash
pip install -r requirements.txt
```

## Utilisation

Lancez l'application avec FastAPI :

```bash
uvicorn LOMBARD_Nicolas_1_modele_012024:app --reload
```

Visitez `http://127.0.0.1:8000` dans votre navigateur.

## Endpoints API

- **POST `/predict/`** : Soumettez un texte pour prédiction.
  
  Exemple de corps de requête :
  ```json
  {
      "text": "Votre texte ici..."
  }
  ```

- **GET `/healthcheck`** : Vérifiez l'état de fonctionnement de l'API.

## Construction du modèle

Le modèle de Deep Learning est construit à l'aide du notebook Jupyter `LOMBARD_Nicolas_2_notebook_012024.ipynb`.
Les données utilisées pour l'entraînement proviennent d'un fichier CSV contenant 1.6 millions de tweets, disponible [ici](https://s3-eu-west-1.amazonaws.com/static.oc-static.com/prod/courses/files/AI+Engineer/Project+7%C2%A0-+D%C3%A9tectez+les+Bad+Buzz+gr%C3%A2ce+au+Deep+Learning/sentiment140.zip).
Le modèle entraîné doit être placé dans un dossier nommé `model_prod`.

## Tests

Lancez les tests à partir de la racine du projet en exécutant :

```bash
pytest test_api.py
```

Cela exécutera les tests définis dans `test_api.py`, vérifiant que les fonctionnalités clés de l'API fonctionnent comme prévu.

## Déploiement avec Docker

Construisez l'image Docker à partir du Dockerfile fourni :

```bash
docker build -t nom_de_votre_image .
```

Lancez le conteneur Docker :

```bash
docker run -d --name nom_du_conteneur -p 8000:80 nom_de_votre_image
```

## Licence

[MIT](https://choosealicense.com/licenses/mit/)