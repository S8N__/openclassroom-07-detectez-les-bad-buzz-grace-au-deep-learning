# FastAPI for creating web applications
from fastapi import FastAPI, HTTPException

# Pydantic for data validation and settings management using Python type annotations
from pydantic import BaseModel

# NLTK (Natural Language Toolkit) for natural language processing tasks
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

# Machine Learning
from tensorflow.keras.models import load_model
from keras.preprocessing.text import tokenizer_from_json
from keras.preprocessing.sequence import pad_sequences

# FastText for efficient learning of word representations and sentence classification
import fasttext

# Standard library imports for various utility functions
import re
import string
import contractions
import os

import emoji
from spellchecker import SpellChecker

from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["POST"],
    allow_headers=["*"],
)

# Load the FastText language detection model
fasttext_model = fasttext.load_model('lid.176.ftz')

mention_regex = re.compile(r'@\w+ ?')
hashtag_regex = re.compile(r'#\w+ ?')
url_regex = re.compile(r'http\S+ ?')
punctuation_regex = re.compile(f'[{re.escape(string.punctuation)}]')


def detect_language(text: str) -> str:
    lang, _ = fasttext_model.predict(text)
    return lang[0].replace('__label__', '')

# Initialize WordNetLemmatizer outside of the function for efficiency
lemmatizer = WordNetLemmatizer()
def remove_stopwords_and_lemmatize(text, stopwords, negative_words):
            return ' '.join([lemmatizer.lemmatize(word) for word in text.split() if word not in stopwords or word in negative_words])

def process_text(text: str, lang: str) -> str:
    """
    Preprocess the given text for NLP tasks.
    
    Parameters:
    - text (str): The text to preprocess.
    - lang (str): The language code of the text.
    
    Returns:
    - str: The preprocessed text.
    """
    spell = SpellChecker(language=lang)

    # Expand contractions (e.g., "don't" to "do not")
    text = contractions.fix(text).lower()
    
    # Remove mentions, hashtags, URLs, and punctuation, then convert to lowercase
    text = re.sub(r'@\w+ ?', 'MENTION ', text)
    text = re.sub(r'#\w+ ?', '', text)
    text = re.sub(r'http\S+ ?', '', text)
    text = re.sub(f'[{string.punctuation}]', '', text)

    text = emoji.demojize(text, delimiters=("", ""))

    # Alpha numeric
    text = re.sub(r'[^\w\s]', '', text)
    
    # Supported languages and their stopwords
    languages = {
        "da": 'danish', "nl": 'dutch', "en": 'english', "fi": 'finnish', 
        "fr": 'french', "de": 'german', "el": 'greek', "it": 'italian', 
        "no": 'norwegian', "pt": 'portuguese', "ru": 'russian', 
        "sl": 'slovene', "es": 'spanish', "sv": 'swedish', "tr": 'turkish'
    }

    negative_words_map = {
        "en": {"not", "never", "nothing", "no", "nor", "however", "yet", "nevertheless", "nonetheless", "but"},
        "fr": {"pas", "jamais", "rien", "aucun", "ni","cependant",'pourtant','néanmoins','toutefois','or','mais'},
        "de": {"nicht", "nie", "nichts", "kein", "noch", "jedoch", "dennoch", "nichtsdestotrotz", "gleichwohl", "aber"},
        "es": {"no", "jamás", "nada", "ningún", "ni", "sin embargo", "no obstante", "sin embargo", "no obstante", "sin embargo","pero"},
        "it": {"non", "mai", "nulla", "nessun", "né", "tuttavia", "però", "nonostante ciò", "tuttavia", "però","ma"},
        "pt": {"não", "nunca", "nada", "nenhum", "nem", "no entanto", "entretanto", "todavia", "no entanto", "porém","mas"},
        "da": {"ikke", "aldrig", "ingenting", "ingen", "hverken", "dog", "alligevel", "ikke desto mindre", "imidlertid", "men"},
        "nl": {"niet", "nooit", "niets", "geen", "noch", "echter", "desalniettemin", "niettemin", "evenwel", "maar"},
        "fi": {"ei", "koskaan", "mitään", "ei mitään", "ei", "kuitenkin", "kuitenkin", "kuitenkin", "kuitenkin", "kuitenkin","mutta"},
        "el": {"όχι", "ποτέ", "τίποτα", "κανένας", "ούτε", "όμως", "παρ' όλα αυτά", "παρ' όλο αυτό", "παρ' όλο αυτό", "αλλά"},
        "no": {"ikke", "aldri", "ingenting", "ingen", "heller ikke", "likevel", "likevel", "likevel", "likevel", "likevel","men"},
        "ru": {"не", "никогда", "ничего", "нет", "ни", "однако", "тем не менее", "однако", "всё же", "но"},
        "sl": {"ne", "nikoli", "nič", "noben", "niti", "vendar", "vendar", "vendar", "vendar", "vendar","ampak"},
        "sv": {"inte", "aldrig", "ingenting", "ingen", "varken", "dock", "ändå", "trots detta", "dock", "men"},
        "tr": {"değil", "asla", "hiçbir şey", "hiçbir", "ne", "ancak", "yine de", "yine de", "yine de", "ama"}
    }

    if lang in languages:
        stop_words = set(stopwords.words(languages[lang]))
        negative_words = negative_words_map.get(lang, set())
        text = ' '.join([word for word in text.split() if word not in stop_words or word in negative_words])
        
        # Remove lemmatize
        text = ' '.join([lemmatizer.lemmatize(word) for word in text.split()])

    # Split text into words and find those that may be misspelled
    words = text.split()
    misspelled = spell.unknown(words)

    corrected_text = []
    for word in words:
        # If the word is misspelled, get the most likely correction
        if word in misspelled:
            corrected_word = spell.correction(word)
            # Only add the corrected word if it is not None
            if corrected_word is not None:
                corrected_text.append(corrected_word)
            else:
                # If no correction is found, add the original word
                corrected_text.append(word)
        else:
            corrected_text.append(word)

    # Join corrected words back into a single string
    return ' '.join(corrected_text)

model = load_model("model_prod")
with open(os.path.join("model_prod", "tokenizer.json"), 'r', encoding='utf-8') as f:
    tokenizer = tokenizer_from_json(f.read())

class TextData(BaseModel):
    text: str

@app.post("/predict/")
async def predict(data: TextData):
    if not data.text:
        raise HTTPException(status_code=400, detail="Text input is required.")

    try:
        lang = detect_language(data.text)
        processed_text = process_text(data.text, lang)
        test_seq = tokenizer.texts_to_sequences(processed_text)
        test_pad = pad_sequences(test_seq, maxlen=512)
        prediction = model.predict([test_pad])[0].item()
        return {
            "prediction": prediction,
            "lang":lang,
            "processed_text":processed_text
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    
@app.get("/healthcheck")
def health_check():
    return {"status": "ok"}