# Environnement de développement
jupyter
pandas

matplotlib
# fasttext

nltk
ftfy
wordcloud
contractions

scikit-learn
mlflow
shap

gensim
keras
tensorflow
seaborn
transformers
accelerate
tensorflow_hub


keras-tuner

emoji